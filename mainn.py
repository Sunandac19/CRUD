import MySQLdb,sqlite3,psycopg2
import sys
import abc
import db_queries
def choice():
    print "1.Mysql"
    print "2.Sqlite3"
    print "3.Postgresql"
    print "4.Quit"
    user = raw_input("Which database you want to access?")
    if user == "4":
        print "\n EXIT"
    elif user:
        global myclassobj
        myclassobj = db_queries.abc(user)
        myclassobj.create()
choice()
def choose():
    while(1):
        print "1. Insert"
        print "2. Update"
        print "3. Delete"
        print "4. Select"
        print "5. Show columns"
        print "6. Delete column"
        print "7. Add column"
        print "8. list databases"
        print "9. list tables"
        print "10. Drop table"
        print "11. show sqlite databases"
        print "12. Show postgresql databases"
        print "13. Show postgresql tables"
        print "14. show sqlite tables"
        print "15. Exit"
        us = raw_input("What you want to do?")
        if us == "1":
            uName = raw_input("Enter User Name:")
            uPass = raw_input("Enter Password : ")
            myclassobj.insert(uName=uName,uPass=uPass)
        elif us == "2":
            uN = raw_input("Enter Name:")
            uP = raw_input("Enter new password:")
            myclassobj.update(uP = uP,uN = uN)
        elif us == "3":
            u = raw_input("Enter name to delete:")
            myclassobj.delete(u = u)
        elif us == "4":
            myclassobj.select()
        elif us == "5":
            myclassobj.show_columns()
        elif us == "6":
            del_col = raw_input("Enter column to delete:")
            myclassobj.delete_column(del_col = del_col)
        elif us == "7":
            col = raw_input("Enter column name to add:")
            myclassobj.alter(col = col)
        elif us == "8":
            myclassobj.list_databases()
        elif us == "9":
            myclassobj.list_tables()
        elif us == "10":
            t = raw_input("Enter table to drop:")
            myclassobj.drop_table(t = t)
        elif us == "11":
            myclassobj.sqlite_list_databases()
        elif us == "12":
            myclassobj.postgresql_list_databases()
        elif us == "13":
            myclassobj.postgresql_list_tables()
        elif us == "14":
            myclassobj.sqlite_list_tables()
        elif us == "15":
            myclassobj.sqlite_list_tables()
        else:
            quit()
choose()