#!/usr/bin/python
import db_config
import sqlite3
import psycopg2
import MySQLdb
import sys
class abc:
	con = 0
	def __init__(self, val):
		if val == '1':
			self.con = db_config.dbmysql()
		if val == '2':
			self.con = db_config.dbsqlite()
		if val == '3':
			self.con = db_config.dbpost()
	def create(self):
		cur = self.con.cursor()
		que = """CREATE TABLE IF NOT EXISTS exam (username text NOT NULL, pswd text NOT NULL );"""
		cur.execute(que)
		self.con.commit()
		print "Table created"
	def insert(self,**kwargs):
		cur = self.con.cursor()
		query = """INSERT INTO exam(username, pswd) VALUES('%s', '%s');""" % (kwargs['uName'],kwargs['uPass'])
		cur.execute(query)
		print "Record inserted"
		self.con.commit()
	def update(self,**kwargs):
		cur = self.con.cursor()
		quer = ("UPDATE exam SET pswd = '{}' WHERE username='{}';").format((kwargs['uP']),kwargs['uN'])
		cur.execute(quer)
		print "Record updated"
		self.con.commit()	
	def delete(self,**kwargs):
		cur = self.con.cursor()
		qq = ("DELETE FROM exam where username='{}';").format(kwargs['u'])
		cur.execute(qq)
		print "Record deleted"
		self.con.commit()
	def select(self, **kwargs):
		cur = self.con.cursor()
		qwer = ("SELECT * FROM exam;")
		cur.execute(qwer)
		rows = cur.fetchall()
		if(len(rows) == None):
			print 'No record found'
		else:
			print rows
		self.con.commit()
	def show_columns(self):
		cur = self.con.cursor()
		s = ("SHOW COLUMNS FROM exam;")
		cur.execute(s)
		columns = cur.fetchall()
		if(columns == None):
			print "No columns"
		else:
			print columns
		self.con.commit()
	def delete_column(self, **kwargs):
		cur = self.con.cursor()
		del_c = ("""ALTER TABLE exam DROP COLUMN {};""").format(kwargs['del_col'])
		cur.execute(del_c)
		print "Column deleted"
		self.con.commit()
	def alter(self, **kwargs):
		cur = self.con.cursor()
		qw = ("""ALTER TABLE exam ADD COLUMN {} text;""").format(kwargs['col'])
		cur.execute(qw)
		self.con.commit()
		print "table altered"
	def list_databases(self):
		cur = self.con.cursor()
		cur.execute('SHOW DATABASES;')
		ss = cur.fetchall()
		if (ss == None):
			print "No databases"
		else:
			print ss
		self.con.commit()
	def list_tables(self):
		cur = self.con.cursor()
		cur.execute('SHOW TABLES;')
		ld = cur.fetchall()
		if (ld == None):
			print "No tables"
		else:
			print ld
		self.con.commit()
	def drop_table(self, **kwargs):
		cur = self.con.cursor()
		d_tab = ('DROP TABLE {};').format(kwargs['t'])
		cur.execute(d_tab)
		self.con.commit()
		print "table droped"
	def sqlite_list_databases(self):
		cur = self.con.cursor()
		cur.execute('PRAGMA database_list;')
		s_d = cur.fetchall()
		if (s_d == None):
			print "No databases"
		else:
			print s_d
	def postgresql_list_databases(self):
		cur = self.con.cursor()
		cur.execute('SELECT datname FROM pg_database;')
		p_d = cur.fetchall()
		if (p_d == None):
			print "No databases"
		else:
			print p_d
	def postgresql_list_tables(self):
		cur = self.con.cursor()
		cur.execute('SELECT table_name FROM information_schema.tables;')
		p_t = cur.fetchall()
		if (p_t == None):
			print "No tables"
		else:
			print p_t
	def sqlite_list_tables(self):
		cur = self.con.cursor()
		cur.execute("SELECT * FROM exam;")
		s_t = cur.fetchall()
		if (s_t == None):
			print "No tables"
		else:
			print s_t